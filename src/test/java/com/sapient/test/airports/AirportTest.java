package com.sapient.test.airports;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.sapient.airport.dao.AirportsDAOImpl;
import com.sapient.airport.dao.IAirportsDAO;
import com.sapient.airport.exceptions.ObjectNotFoundException;
import com.sapient.airports.vo.Airport;

@DisplayName("Airports Tests")
class AirportTest {

	IAirportsDAO dao = null;

	@BeforeEach()
	void initTest() {
		dao = new AirportsDAOImpl();
	}

	@DisplayName("Find Airport By Name")
	@Test
	void testFindAirportByName() {
		String airportName = "Lowell Field";
		List<Airport> airports = dao.findByName(airportName);
		assertFalse(airports.isEmpty());
	}

	@DisplayName("Find Airport By Name Bad Args")
	@Test
	void testFindAirportByNameBadArgs() {
		assertThrows(IllegalArgumentException.class, () -> dao.findByName(null));
	}

	@DisplayName("Find Airport By Type")
	@Test
	void testFindAirportByType() {
		String airportType = "small_airport";
		List<Airport> airports = dao.findByType(airportType);
		assertFalse(airports.isEmpty());
	}

	@DisplayName("Find Airport By Type Badargs")
	@Test
	void testFindAirportByTypeBadArgs() {
		assertThrows(IllegalArgumentException.class, () -> dao.findByType(null));
	}

	@DisplayName("Find Airport By City")
	@Test
	void testFindAirportByCity() {
		String city = "Leoti";
		List<Airport> airports = dao.findByCity(city);
		assertFalse(airports.isEmpty());
	}

	@DisplayName("Find Airport By City Bad Args")
	@Test
	void testFindAirportByCityBadArgs() {
		assertThrows(IllegalArgumentException.class, () -> dao.findByType(null));
	}

	@DisplayName("Find Airport By Address")
	@Test
	@Disabled("No Clarity on Requirements")
	void testFindAirportByAddress() {
		fail("not yet implemented");
	}

	@DisplayName("Find Airport By Lat/Long")
	@Test
	void testFindAirportByLatLong() {
		String lat = "59.94919968";
		String lng = "-151.695999146";
		List<Airport> airports = dao.findByLatLong(lat, lng);
		assertFalse(airports.isEmpty());
	}

	@DisplayName("Find Airport By NearMe")
	@Test
	void testFindAirportNearMe() {
		String lat = "59.94919968";
		String lng = "-151.695999146";
		List<Airport> airports = dao.findByLatLong(lat, lng);
		assertFalse(airports.isEmpty());
	}

	@DisplayName("Find Airport Randomly")
	@Test
	void testFindAirportRandomly() {
		String airportName = "Lowell Field";
		List<Airport> airports = dao.findByName(airportName);
		assertFalse(airports.isEmpty());
	}

	@DisplayName("Count of All Airports")
	@Test
	void testCountOfAirports() {
		assertEquals(66601, dao.count());
	}

	@DisplayName("List All Airports")
	@Test
	void testListAllAirports() {
		List<Airport> airports = dao.findAll();
		assertEquals(66601, airports.size());
	}

	@DisplayName("Test Find By Id")
	@Test
	void testFindAirportById() {
		assertNotNull(dao.findById(6524L));
	}

	@DisplayName("Test Find By Id BadArgs")
	@Test
	void testFindAirportByIdBadParams() {
		assertThrows(IllegalArgumentException.class, () -> dao.findById(null));
	}

	@DisplayName("Test Find By Id ObjectNotFound Exception")
	@Test
	void testFindAirportByIdNotFound() {
		assertThrows(ObjectNotFoundException.class, () -> dao.findById(-1L));
	}

}
