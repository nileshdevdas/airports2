package com.sapient.test.airports;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.sapient.airports.functions.TransformerFunctions;
import com.sapient.airports.vo.Airport;
import com.sapient.airports.vo.Country;
import com.sapient.airports.vo.NavAid;
import com.sapient.airports.vo.Region;

class TransformerTest {

	@BeforeEach
	void init() {
		// TODO
	}

	@DisplayName("Test String is converted to Airport")
	@Test
	void testStringToAirport() {
		Airport airport = TransformerFunctions.stringToAirport(
				"6524,\"00AK\",\"small_airport\",\"Lowell Field\",59.94919968,-151.695999146,450,\"NA\",\"US\",\"US-AK\",\"Anchor Point\",\"no\",\"00AK\",,\"00AK\",,,");
		assertNotNull(airport);
		assertEquals("Lowell Field", airport.getName());
	}

	@DisplayName("Test String is converted to Airport BadArgs")
	@Test
	void testStringToAirportBadArgs() {
		assertThrows(IllegalArgumentException.class, () -> TransformerFunctions.stringToAirport(null));
	}

	@DisplayName("Test String is converted to Country")
	@Test
	void testStringToCountry() {
		Country country = TransformerFunctions
				.stringToCountry("302672,\"AD\",\"Andorra\",\"EU\",\"https://en.wikipedia.org/wiki/Andorra\",");
		assertNotNull(country);
		assertEquals("Andorra", country.getName());
	}

	@DisplayName("Test String is converted to Country BadArgs")
	@Test
	void testStringToCountryBadArgs() {
		assertThrows(IllegalArgumentException.class, () -> TransformerFunctions.stringToCountry(null));
	}

	@DisplayName("Test String is converted to Naviaid")
	@Test
	void testStringToNavaid() {
		NavAid navaid = TransformerFunctions.stringToNavaid(
				"85050,\"Williams_Harbour_NDB_CA\",\"1A\",\"Williams Harbour\",\"NDB\",373,52.55889892578125,-55.78219985961914,70,\"CA\",,,,,,,-23.072,\"LO\",\"MEDIUM\",\"CCA6\"");
		assertNotNull(navaid);
		assertEquals("Williams Harbour", navaid.getName());
	}

	@DisplayName("Test String is converted to Navaid BadArgs")
	@Test
	void testStringToNavaidBadArgs() {
		assertThrows(IllegalArgumentException.class, () -> TransformerFunctions.stringToNavaid(null));
	}

	@DisplayName("Test String is converted to Region")
	@Test
	void testStringToRegion() {
		Region region = TransformerFunctions.stringToRegion(
				"302811,\"AD-02\",02,\"Canillo Parish\",\"EU\",\"AD\",\"https://en.wikipedia.org/wiki/Canillo\",");
		assertNotNull(region);
		assertEquals("Canillo Parish", region.getName());
	}

	@DisplayName("Test String is converted to Region BadArgs")
	@Test
	void testStringToRegionBadArgs() {
		assertThrows(IllegalArgumentException.class, () -> TransformerFunctions.stringToRegion(null));
	}

	@AfterEach
	void destroy() {
		// TODO
	}
}
