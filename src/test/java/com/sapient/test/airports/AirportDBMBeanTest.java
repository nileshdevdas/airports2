package com.sapient.test.airports;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Date;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.sapient.airport.instrumentation.AirportDBMBean;

class AirportDBMBeanTest {

	@Test
	void testGetDBSize() {
		assertNotEquals(0L, new AirportDBMBean().getDBSize());
	}

	@Test
	void testGetLastLoaded() {
		assertNotNull(new AirportDBMBean().getLastLoaded());
	}

	@Test
	void testReloadData() {
		AirportDBMBean mbean = new AirportDBMBean();
		Date lastLoaded = mbean.getLastLoaded();
		mbean.reloadData();
		Date newLoaded = mbean.getLastLoaded();
		assertNotEquals(lastLoaded, newLoaded);
	}

	@Test
	@Disabled("Cannot Test as will shutdown app")
	@DisplayName("Shutdown Test")
	void testShutdown() {
		fail("Not yet implemented");
	}

}
