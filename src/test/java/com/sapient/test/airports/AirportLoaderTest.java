package com.sapient.test.airports;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.sapient.airport.db.AirportsInMemoryDB;
import com.sapient.airports.vo.Airport;

class AirportLoaderTest {

	@BeforeEach
	void init() {
	}

	@DisplayName("Load Airport From CSV")
	@Test
	void testLoadAirport() {
		List<Airport> airports = AirportsInMemoryDB.airports();
		assertNotNull(airports);
	}
}