package com.sapient.airport.helper;

public class Validators {

	private Validators() {
	}

	public static void checkNullOrEmpty(String param) {
		if (param == null || param.length() == 0)
			throw new IllegalArgumentException();
	}

	public static void checkNullOrZero(Long param) {
		if (param == null || param == 0)
			throw new IllegalArgumentException();
	}
}
