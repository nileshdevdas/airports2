package com.sapient.airport.helper;

public enum Constants {
	AIRPORTS_CSV, REGIONS_CSV, NAVAIDS_CSV, COUNTRIES_CSV
}
