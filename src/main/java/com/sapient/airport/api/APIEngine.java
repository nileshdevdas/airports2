package com.sapient.airport.api;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class APIEngine extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1367467890564038284L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/plain");
		resp.getWriter().write(new VersionAPI().version());
	}
}
