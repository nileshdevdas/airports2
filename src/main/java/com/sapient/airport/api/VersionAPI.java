package com.sapient.airport.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VersionAPI {
	private static Logger logger = LoggerFactory.getLogger(VersionAPI.class);

	public String version() {
		logger.info("Entered::version");
		return "1.0";
	}
}
