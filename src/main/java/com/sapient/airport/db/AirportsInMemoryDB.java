package com.sapient.airport.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.airport.exceptions.DBIntializationException;
import com.sapient.airport.helper.Constants;
import com.sapient.airport.helper.PropertyHelper;
import com.sapient.airports.functions.TransformerFunctions;
import com.sapient.airports.vo.Airport;
import com.sapient.airports.vo.Country;
import com.sapient.airports.vo.NavAid;
import com.sapient.airports.vo.Region;

/**
 * @author Nilesh Devdas
 * @version 1.0
 * @since 1.0
 */
public class AirportsInMemoryDB {
	private static final Logger logger = LoggerFactory.getLogger(AirportsInMemoryDB.class);
	private static List<Airport> airportsDB = null;
	private static List<Country> countriesDB = null;
	private static List<Region> regionsDB = null;
	private static List<NavAid> naviaidsDB = null;
	private static Date lastLoaded;

	private AirportsInMemoryDB() {
	}

	public static void setLastLoaded(Date lastLoaded) {
		AirportsInMemoryDB.lastLoaded = lastLoaded;
	}

	public static Date getLastLoaded() {
		return lastLoaded;
	}

	static {
		logger.info("Loading Database ");
		loadDatabase();
		logger.info("Loaded Database");

	}

	public static void loadDatabase() {
		try {
			loadAirports();
			loadRegions();
			loadCountries();
			loadNavaids();
			// TODO by you
			// loadfrequencies
			// loadrunways
			setLastLoaded(new Date(System.currentTimeMillis()));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	private static void loadNavaids() throws IOException {
		naviaidsDB = Files.readString(Paths.get(PropertyHelper.getProperty(Constants.NAVAIDS_CSV.name()))).lines()
				.skip(1).map(TransformerFunctions::stringToNavaid).collect(Collectors.toList());
		if (naviaidsDB != null && logger.isInfoEnabled())
			logger.info(String.format("NavAids Database Loaded with %s records", naviaidsDB.size()));
		else
			throw new DBIntializationException();
	}

	private static void loadCountries() throws IOException {
		countriesDB = Files.readString(Paths.get(PropertyHelper.getProperty(Constants.COUNTRIES_CSV.name()))).lines()
				.skip(1).map(TransformerFunctions::stringToCountry).collect(Collectors.toList());
		if (countriesDB != null && logger.isInfoEnabled())
			logger.info(String.format("Countries Database Loaded with %s records", countriesDB.size()));
	}

	private static void loadRegions() throws IOException {
		regionsDB = Files.readString(Paths.get(PropertyHelper.getProperty(Constants.REGIONS_CSV.name()))).lines()
				.skip(1).map(TransformerFunctions::stringToRegion).collect(Collectors.toList());
		if (regionsDB != null && logger.isInfoEnabled())
			logger.info(String.format("Regions Database Loaded with %s records", regionsDB.size()));
	}

	private static void loadAirports() throws IOException {
		airportsDB = Files.readString(Paths.get(PropertyHelper.getProperty(Constants.AIRPORTS_CSV.name()))).lines()
				.skip(1).map(TransformerFunctions::stringToAirport).collect(Collectors.toList());
		if (airportsDB != null && logger.isInfoEnabled())
			logger.info(String.format("Airports Database Loaded with %s records", airportsDB.size()));
	}

	/**
	 * @return
	 */

	public static synchronized List<Airport> airports() {
		return airportsDB;
	}

	/**
	 * @return
	 */
	public static List<Country> countries() {
		return countriesDB;
	}

	/**
	 * @return
	 */
	public static List<NavAid> navaids() {
		return naviaidsDB;
	}

	/**
	 * @return
	 */
	public static List<Region> regions() {
		return regionsDB;
	}
}
