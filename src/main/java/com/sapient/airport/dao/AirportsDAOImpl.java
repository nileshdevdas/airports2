package com.sapient.airport.dao;

import java.util.List;
import java.util.stream.Collectors;

import com.sapient.airport.db.AirportsInMemoryDB;
import com.sapient.airport.exceptions.ObjectNotFoundException;
import com.sapient.airport.helper.Validators;
import com.sapient.airports.vo.Airport;

/**
 * Class under test
 * 
 * @author nilesh
 *
 */
public class AirportsDAOImpl implements IAirportsDAO {

	/**
	 * @return
	 */
	@Override
	public List<Airport> findAll() {
		return AirportsInMemoryDB.airports();
	}

	/**
	 * @param country
	 * @return
	 */
	@Override
	public List<Airport> findByCountry(String country) {
		Validators.checkNullOrEmpty(country);
		return AirportsInMemoryDB.airports().stream().filter(r -> r.getCountryName().equals(country))
				.collect(Collectors.toList());
	}

	/**
	 * @param id
	 * @return
	 */
	@Override
	public Airport findById(Long id) {
		Validators.checkNullOrZero(id);
		return AirportsInMemoryDB.airports().stream().filter(r -> r.getId().equals(id)).findFirst()
				.orElseThrow(ObjectNotFoundException::new);
	}

	/**
	 * @param name AirportName
	 * @return
	 */
	@Override
	public List<Airport> findByName(String name) {
		Validators.checkNullOrEmpty(name);
		return AirportsInMemoryDB.airports().stream().filter(r -> r.getName().contains(name))
				.collect(Collectors.toList());
	}

	/**
	 * @param type
	 * @return
	 */
	@Override
	public List<Airport> findByType(String type) {
		Validators.checkNullOrEmpty(type);
		return AirportsInMemoryDB.airports().stream().filter(r -> r.getType().equals(type))
				.collect(Collectors.toList());
	}

	@Override
	public List<Airport> findByCity(String name) {
		Validators.checkNullOrEmpty(name);
		return AirportsInMemoryDB.airports().stream().filter(r -> r.getMunicipality().equals(name))
				.collect(Collectors.toList());
	}

	@Override
	public int count() {
		return AirportsInMemoryDB.airports().size();
	}

	@Override
	public List<Airport> findByLatLong(String lat, String lng) {
		Validators.checkNullOrEmpty(lat);
		Validators.checkNullOrEmpty(lng);
		return AirportsInMemoryDB.airports().stream()
				.filter(r -> r.getLatitude().equals(lat) && r.getLongitude().equals(lng)).collect(Collectors.toList());
	}

}