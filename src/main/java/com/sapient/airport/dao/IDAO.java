package com.sapient.airport.dao;

import java.util.List;

public interface IDAO<T> {
	List<T> findAll();

	int count();

	T findById(Long id);
}
