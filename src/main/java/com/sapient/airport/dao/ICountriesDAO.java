package com.sapient.airport.dao;

import java.util.List;

import com.sapient.airports.vo.Country;

/**
 * DAO to be implemented by you
 * 
 * @author niles
 *
 */
public interface ICountriesDAO extends IDAO<Country> {

	Country findByCode(String code);

	List<Country> findByContinent(String name);

	List<Country> findByName(String name);
}
