package com.sapient.airport.dao;

import java.util.List;

import com.sapient.airports.vo.Airport;

/**
 * program with interfaces ---> a) you can mock b) you can have the closed but
 * open requirements --> c) the Test cases generated in tooling sense d) the
 * implementation can vary but the requirement is conformance
 */
public interface IAirportsDAO extends IDAO<Airport> {
	List<Airport> findByCountry(String country);

	List<Airport> findByName(String name);

	List<Airport> findByType(String name);

	List<Airport> findByCity(String name);

	List<Airport> findByLatLong(String lat, String lng);
}
