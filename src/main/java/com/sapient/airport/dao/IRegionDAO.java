package com.sapient.airport.dao;

import java.util.List;

import com.sapient.airports.vo.Region;

/**
 * DAO to be implemented by you
 * 
 * @author niles
 *
 */
public interface IRegionDAO extends IDAO<Region> {
	Region findByCode(String code);

	List<Region> findByContinent(String continent);

	List<Region> findByName(String name);
}
