package com.sapient.airport.dao;

import java.util.List;

import com.sapient.airports.vo.NavAid;

/**
 * DAO to be implemented by you
 * 
 * @author niles
 *
 */
public interface INavAidsDAO extends IDAO<NavAid> {

	List<NavAid> findByName(String name);
}
