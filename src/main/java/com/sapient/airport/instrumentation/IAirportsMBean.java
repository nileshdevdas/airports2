package com.sapient.airport.instrumentation;

import java.util.Date;

public interface IAirportsMBean {
	/**
	 * @return how many records DB
	 */
	public int getDBSize();

	/**
	 * When was the data loaded
	 * 
	 * @return
	 */
	public Date getLastLoaded();

	/**
	 * Operation where you wish to reload the data on a call
	 */
	public void reloadData();

	/**
	 * Action to Shutdown your application
	 */
	public void shutdown();

	public void clearData();
}
