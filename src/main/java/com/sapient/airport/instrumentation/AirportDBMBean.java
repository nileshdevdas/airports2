package com.sapient.airport.instrumentation;

import java.util.Date;

import com.sapient.airport.db.AirportsInMemoryDB;

public class AirportDBMBean implements IAirportsMBean {
	public int getDBSize() {
		return AirportsInMemoryDB.airports().size() + AirportsInMemoryDB.countries().size()
				+ AirportsInMemoryDB.regions().size() + AirportsInMemoryDB.navaids().size();
	}

	public Date getLastLoaded() {
		return AirportsInMemoryDB.getLastLoaded();
	}

	public void reloadData() {
		AirportsInMemoryDB.loadDatabase();
	}

	public void shutdown() {
		System.exit(0);
	}

	public void clearData() {
		AirportsInMemoryDB.airports().clear();
		AirportsInMemoryDB.navaids().clear();
		AirportsInMemoryDB.countries().clear();
		AirportsInMemoryDB.regions().clear();
	}
}
