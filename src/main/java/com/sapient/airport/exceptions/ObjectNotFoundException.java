package com.sapient.airport.exceptions;

public class ObjectNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -5924614731510483809L;
}
