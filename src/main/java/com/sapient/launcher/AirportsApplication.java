package com.sapient.launcher;

import java.lang.management.ManagementFactory;

import javax.management.ObjectName;
import javax.management.StandardMBean;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.airport.api.APIEngine;
import com.sapient.airport.helper.PropertyHelper;
import com.sapient.airport.instrumentation.AirportDBMBean;
import com.sapient.airport.instrumentation.IAirportsMBean;

public class AirportsApplication {
	/**
	 * Logger for logging the application Startup behaviours ++ Shutdown Hook
	 */
	private static final Logger logger = LoggerFactory.getLogger(AirportsApplication.class);
	static {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				// TODO you will have to cleanup all the resources if you engaging resource
				logger.info("Shutting Down Application");
			}
		});
	}

	public static void main(String[] args) {
		registerMBeans();
		startJetty();
	}

	private static void startJetty() {
		var server = new Server();
		try (var connector = new ServerConnector(server)) {
			connector.setPort(Integer.valueOf(PropertyHelper.getProperty("server.port")));
			server.setConnectors(new Connector[] { connector });
			var context = new ServletContextHandler(ServletContextHandler.SESSIONS);
			context.setContextPath("/api");
			server.setHandler(context);
			context.addServlet(new ServletHolder(new APIEngine()), "/*");
			server.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * To register any Class that you wish to expose as Management Bean This bean
	 * can be controlled using the The Java
	 */
	private static void registerMBeans() {
		/**
		 * I have 1 bean Ready to be served via Jconsole
		 */
		try {
			ManagementFactory.getPlatformMBeanServer().registerMBean(
					new StandardMBean(new AirportDBMBean(), IAirportsMBean.class),
					new ObjectName("com.sapient.airports:type=DatabaseManager"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
