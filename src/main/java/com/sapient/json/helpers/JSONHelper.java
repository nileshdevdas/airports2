package com.sapient.json.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sapient.airports.vo.Airport;

public class JSONHelper {
	public static void main(String[] args) throws Exception {
		Airport airport = new Airport();
		airport.setId(1L);
		airport.setName("XXX");
		ObjectMapper mapper = new ObjectMapper();
		String str = "{\"id\":1,\"name\":\"XXX\",\"ident\":null,\"type\":null,\"latitude\":null,\"longitude\":null,\"countryName\":null,\"regionName\":null,\"municipality\":null}\r\n"
				+ "";
		Airport airportP = mapper.readValue(str, Airport.class);
	}
}
