package com.sapient.airports.vo;

public class Airport {
	private Long id;
	private String name;
	private String ident;
	private String type;
	private String latitude;
	private String longitude;
	private String countryName;
	private String regionName;
	private String municipality;

	public String getCountryName() {
		return countryName;
	}

	public Long getId() {
		return id;
	}

	public String getIdent() {
		return ident;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getMunicipality() {
		return municipality;
	}

	public String getName() {
		return name;
	}

	public String getRegionName() {
		return regionName;
	}

	public String getType() {
		return type;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIdent(String ident) {
		this.ident = ident;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public void setType(String type) {
		this.type = type;
	}
}
