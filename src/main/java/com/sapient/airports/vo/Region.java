package com.sapient.airports.vo;

public class Region {

	private Long id;
	private String code;
	private String name;
	private String continent;
	private String isoCountry;

	public String getCode() {
		return code;
	}

	public String getContinent() {
		return continent;
	}

	public Long getId() {
		return id;
	}

	public String getIsoCountry() {
		return isoCountry;
	}

	public String getName() {
		return name;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIsoCountry(String isoCountry) {
		this.isoCountry = isoCountry;
	}

	public void setName(String name) {
		this.name = name;
	}

}
